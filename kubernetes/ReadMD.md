# 개요
jenkins에서 ci/cd를 만들경우 프로젝트를 빌드하고 이미지화 하는데 매우 오랜 시간이 걸린다.   
따라서 이미지를 사전에 빌드하고 등록한 후 kubernetes에서 생성된 이미지를 기반으로 pod를 생성하는것을 추천한다.
### 1. 전자정부 프레임워크 이미지 만들기
젠킨스에서 처리하기에는 시간이 너무 많이 걸리므로 이미지를 생성한 후 그 이미지를 배포하는 방식으로 처리함
```shell


docker build --tag docker.io/nextgencloud/egovframework:1.2 .

docker push docker.io/nextgencloud/egovframework:1.2
docker tag docker.io/nextgencloud/egovframework:1.2 docker.io/nextgencloud/egovframework:latest

docker push docker.io/nextgencloud/egovframework:latest

## docker test

docker run -d --name="my-egov" -p 10000:8080 nextgencloud/egovframework:1.0

## IKS 배포 하기 

```

### 2. IKS 배포 하기 
namespace는 m2m-private을 생성 후 진행 합니다.
```bash
kubectl apply -f ./kubernetes/deployment.yaml


