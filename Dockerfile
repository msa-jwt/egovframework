FROM nextgencloud/tomcat:8.5-openjdk8


ENV CATALINA_HOME /usr/local/tomcat
ENV PATH $CATALINA_HOME/bin:$PATH
RUN mkdir -p "$CATALINA_HOME"
WORKDIR $CATALINA_HOME

ENV VERTICLE_FILE ROOT.war
# Set the location of the verticles
ENV VERTICLE_ROOT_DIR $CATALINA_HOME/webapps/


COPY ./target/$VERTICLE_FILE $VERTICLE_ROOT_DIR

EXPOSE 8080
CMD ["catalina.sh", "run"]