# egovframework
## eclipse 초기화
프로젝트를 이클립스에 import하고 프로젝트를 선택후 maven update를 해 주어야 함   
아래 페이지에서 import부분만 참조   
https://www.egovframe.go.kr/wiki/doku.php?id=egovframework:dev3.9:gettingstarted


```shell
# git clone
https://nextgencloud@gitlab.com/msa-jwt/egovframework.git

# git clone후에 계정을 넣고 싶을때
git remote -v
git remote remove origin 
https://nextgencloud@gitlab.com/msa-jwt/egovframework.git
```

## tools 설정
### 이클립스 lombok 설정
lombok을 사용하기 위해서는 2가지 설정이 필요하다. 첫째는 이클립스툴에 설정하는 것이고 두번째는 프로젝트에 lombok.jar를 추가해 주어야 한다.
이클립스 설정   
https://kamsi76.tistory.com/entry/Eclipse-Lombok-%EC%84%A4%EC%A0%95

### 아파치 설정
이클립스를 설치하고 아파치를 설정해 주어야 한다.
아파치틑 사이트에서 zip 버전을 다운하다.
다음 사이트 참조   
https://hyunah030.tistory.com/2


### egov db 초기화 하기
./init-db/ReadME.md 참조

### kubernetes에 배포하기
